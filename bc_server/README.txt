bc_server README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/bin/initialize_bc_server_db development.ini

- $VENV/bin/pserve development.ini

