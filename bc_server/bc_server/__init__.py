from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator

from .security import groupfinder
from .views import utils

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings, root_factory='.resources.Root')
    config.include('pyramid_jinja2')
    config.include('.models')
    config.include('.routes')
    config.commit()

    jinja2_env = config.get_jinja2_environment()
    jinja2_env.filters['datetime'] = utils.format_datetime
    jinja2_env.filters['date'] = utils.format_date

    # Security policies
    authn_policy = AuthTktAuthenticationPolicy(
        settings['bc_server.secret'], callback=groupfinder,
        hashalg='sha512')
    authz_policy = ACLAuthorizationPolicy()
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)

    config.scan()
    return config.make_wsgi_app()
