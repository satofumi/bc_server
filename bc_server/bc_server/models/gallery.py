from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    Boolean,
    DateTime,
)

from .meta import Base
from datetime import datetime


class Gallery(Base):
    __tablename__ = 'galleries'
    gallery_id = Column(Integer, primary_key=True)
    user_hash = Column(Text)
    index = Column(Integer)
    datetime = Column(DateTime, default=datetime.now)
    title = Column(Text)
    description = Column(Text)
    # game mode (normal, creative, endless)
    # difficulty
    # for_siege
    # for_infiltration
    # nice count
    # view count
    # download count
