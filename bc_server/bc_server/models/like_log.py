from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime,
)

from .meta import Base
from datetime import datetime


class LikeLog(Base):
    __tablename__ = 'likelogs'
    ranking_tag = Column(Text, primary_key=True)
    from_user_hash = Column(Text, primary_key=True)
    to_user_hash = Column(Text, primary_key=True)
