from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime,
    Boolean,
)

from .meta import Base
from datetime import datetime


class Ranking(Base):
    __tablename__ = 'ranks'
    ranking_tag = Column(Text, primary_key=True)
    created_at = Column(DateTime, default=datetime.now)
    title = Column(Text)
    description = Column(Text)
    open_datetime = Column(DateTime)
    start_datetime = Column(DateTime)
    end_datetime = Column(DateTime)
    close_datetime = Column(DateTime)
    is_after_download = Column(Boolean(create_constraint=False), default=True, nullable=False)
