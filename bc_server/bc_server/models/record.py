from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime,
)

from .meta import Base
from datetime import datetime


class Record(Base):
    __tablename__ = 'records'
    ranking_tag = Column(Text, primary_key=True)
    user_hash = Column(Text, primary_key=True)
    datetime = Column(DateTime, default=datetime.now)
    round_score = Column(Integer)
    like_count = Column(Integer, default=0)
