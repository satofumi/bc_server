from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime,
)

from .meta import Base
from datetime import datetime


class Token(Base):
    __tablename__ = 'tokens'
    user_hash = Column(Text, primary_key=True)
    datetime = Column(DateTime, default=datetime.now)
    value = Column(Text)
