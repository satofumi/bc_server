from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime,
    Boolean,
)

from .meta import Base
from datetime import datetime


class User(Base):
    __tablename__ = 'users'
    user_id = Column(Integer, primary_key=True, autoincrement=True)
    datetime = Column(DateTime, default=datetime.now)
    user_hash = Column(Text)
    passphrase = Column(Text)
    nickname = Column(Text)
    is_active = Column(Boolean(create_constraint=False), default=True, nullable=False)
    is_admin = Column(Boolean(create_constraint=False), default=False, nullable=False)
