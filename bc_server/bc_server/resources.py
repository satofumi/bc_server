from pyramid.security import Allow, Everyone


class Root(object):
    __acl__ = [(Allow, Everyone, 'viewer'),
               (Allow, 'group:members', 'member'),
               (Allow, 'group:editors', 'editor')]

    def __init__(self, request):
        pass
