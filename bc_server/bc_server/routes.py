def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)

    # http
    config.add_route('home', '/')

    config.add_route('user_login', '/user/login')
    config.add_route('user_logout', '/user/logout')

    config.add_route('ranking', '/ranking')

    config.add_route('ranking_new', '/ranking/new')
    config.add_route('ranking_add', '/ranking/add')

    # text, json
    config.add_route('user_add', '/bc/user/add')
    config.add_route('user_token', '/bc/user/token')
    config.add_route('user_nickname', '/bc/user/nickname')
    config.add_route('user_nickname_update', '/bc/user/nickname/update')

    config.add_route('ranking_list', '/bc/ranking/list')
    config.add_route('ranking_information', '/bc/ranking/{ranking_tag}')
    config.add_route('ranking_upload', '/bc/ranking/{ranking_tag}/upload')
    config.add_route('ranking_map', '/bc/ranking/{ranking_tag}/map')
    config.add_route('ranking_dat', '/bc/ranking/{ranking_tag}/{user_hash}/dat')
    config.add_route('ranking_rep', '/bc/ranking/{ranking_tag}/{user_hash}/rep')
    config.add_route('ranking_likes', '/bc/ranking/{ranking_tag}/likes')
    config.add_route('ranking_like', '/bc/ranking/{ranking_tag}/like')
    config.add_route('ranking_dislike', '/bc/ranking/{ranking_tag}/dislike')
