import os
import sys
import transaction
import time

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from pyramid.scripts.common import parse_vars

from ..models.meta import Base
from ..models import (
    get_engine,
    get_session_factory,
    get_tm_session,
    )
from datetime import (
    datetime,
    timedelta,
    )
from ..models import User
from ..models import Ranking
from ..models import Record


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def _flush_and_sleep(dbsession):
    dbsession.flush()
    time.sleep(0.001)


def _add_dummy_ranking_data(dbsession):
    scores = [0, 10, 5, 5, 4, 3, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    likes = [0, 1, 2, 3, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    # add user
    for i in range(len(scores)):
        user = User(user_hash='ABC{:03}'.format(i), passphrase='dummy', nickname='User {:03}'.format(i), is_active=True, is_admin=False)
        dbsession.add(user)

    # add ranking
    now = datetime.now()
    ranking = Ranking(ranking_tag='RANKING000',
                      title='title 000',
                      description='description 000',
                      open_datetime=now - timedelta(days=2),
                      start_datetime=now - timedelta(days=1),
                      end_datetime=now + timedelta(days=1),
                      close_datetime=now + timedelta(days=2))
    dbsession.add(ranking)
    _flush_and_sleep(dbsession)

    ranking = Ranking(ranking_tag='RANKING001',
                      title='title 001',
                      description='description 001',
                      open_datetime=now - timedelta(days=3),
                      start_datetime=now - timedelta(days=2),
                      end_datetime=now - timedelta(days=1),
                      close_datetime=now + timedelta(days=1))
    dbsession.add(ranking)
    _flush_and_sleep(dbsession)

    ranking = Ranking(ranking_tag='RANKING002',
                      title='title 002',
                      description='description 002',
                      open_datetime=now - timedelta(days=4),
                      start_datetime=now - timedelta(days=3),
                      end_datetime=now - timedelta(days=2),
                      close_datetime=now - timedelta(days=1))
    dbsession.add(ranking)
    _flush_and_sleep(dbsession)

    ranking = Ranking(ranking_tag='RANKING003',
                      title='title 003',
                      description='description 003',
                      open_datetime=now + timedelta(days=1),
                      start_datetime=now + timedelta(days=2),
                      end_datetime=now + timedelta(days=3),
                      close_datetime=now + timedelta(days=4))
    dbsession.add(ranking)
    _flush_and_sleep(dbsession)

    ranking = Ranking(ranking_tag='RANKING004',
                      title='title 004',
                      description='description 004',
                      open_datetime=now - timedelta(days=2),
                      start_datetime=now - timedelta(days=1),
                      end_datetime=now + timedelta(days=1),
                      close_datetime=now + timedelta(days=2),
                      is_after_download=False)
    dbsession.add(ranking)
    _flush_and_sleep(dbsession)

    # add records
    index = 0
    for score in scores:
        like = likes[index]
        record = Record(ranking_tag='RANKING000',
                        user_hash='ABC{:03}'.format(index), round_score=score, like_count=like)
        dbsession.add(record)
        index = index + 1

    record = Record(ranking_tag='RANKING001', user_hash='ABC001', round_score=1)
    dbsession.add(record)

    record = Record(ranking_tag='RANKING004', user_hash='ABC004', round_score=4)
    dbsession.add(record)


def main(argv=sys.argv):
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options)

    engine = get_engine(settings)
    Base.metadata.create_all(engine)

    session_factory = get_session_factory(engine)

    with transaction.manager:
        dbsession = get_tm_session(session_factory, transaction.manager)

        if config_uri.endswith('development.ini'):
            _add_dummy_ranking_data(dbsession)
