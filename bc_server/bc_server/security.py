import bcrypt
import hashlib
import base64
from .models import (
    User,
)


def hash_password(pw):
    pwhash = bcrypt.hashpw(pw.encode('utf8'), bcrypt.gensalt())
    return pwhash.decode('utf8')

def check_password(user_hash, password, dbsession):
    user = dbsession.query(User).filter_by(user_hash=user_hash).one_or_none()
    if not user:
        return False

    db_password = user.passphrase
    hashed = hashlib.sha256((password + user_hash).encode()).digest()
    passphrase = base64.b64encode(hashed)
    return bcrypt.checkpw(passphrase, db_password)


GROUPS = {'editor': ['group:editors', 'group:members'],
          'member': ['group:members']}


def groupfinder(user_hash, request):
    user = request.dbsession.query(User).filter_by(user_hash=user_hash).one_or_none()
    if user:
        return GROUPS.get('editor') if user.is_admin and user.is_active else GROUPS.get('member')

    return []
