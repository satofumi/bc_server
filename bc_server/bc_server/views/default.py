# -*- coding: utf-8 -*-
from pyramid.response import Response
from pyramid.view import view_config
from pyramid.security import (
    remember,
    forget,
    )

from sqlalchemy.exc import DBAPIError
from sqlalchemy import desc

from pyramid.httpexceptions import (
    HTTPFound, HTTPNotFound,
)

from ..models import Token
from ..models import User
from ..models import Ranking
from ..models import Record
from ..models import Gallery
from ..models import LikeLog

from datetime import (
    datetime,
    timedelta,
    )
import json
import yaml
import os
import shutil
import random
import string
import base64
import bcrypt
from Crypto.Cipher import AES
from Crypto.Util import Padding
import dateutil.parser

from ..security import (
    check_password,
)

Server_ok = 'OK'
Rankings_directory = 'rankings/'


def _check_passphrase(dbsession, user_hash, passphrase):
    user = dbsession.query(User).filter_by(user_hash=user_hash).one_or_none()
    return user != None and bcrypt.checkpw(passphrase, user.passphrase)


def _authenticate(dbsession, user_hash, encrypted_passphrase):
    result = dbsession.query(Token).filter_by(user_hash=user_hash).one_or_none()
    if not result:
        return False
    token = result.value

    iv = token[0:16].encode('utf8')
    key = token[16:16+32].encode('utf8')
    cipher = AES.new(key, AES.MODE_CBC, iv)
    passphrase = Padding.unpad(cipher.decrypt(encrypted_passphrase), AES.block_size, 'pkcs7')

    return _check_passphrase(dbsession, user_hash, passphrase)


def _random_token(n):
    return ''.join(random.choices(string.ascii_letters + string.digits, k=n))


def _save_uploaded_file(file_data, path):
    temp_path = path + '~'
    with open(temp_path, 'wb') as f:
        shutil.copyfileobj(file_data, f)
    if os.path.exists(path):
        os.remove(path)
    os.rename(temp_path, path)


def _save_dat_with_tag(dat_file, path, tag):
    lines = dat_file.readlines()
    with open(path, 'w') as f:
        for line in lines:
            line = line.decode('utf8')
            if line.startswith('Difficulty:'):
                f.write('Difficulty: 4\n')
                f.write('RankingTag: ' + tag + '\n')
            else:
                f.write(line.rstrip() + '\n')


def _ranking_records(dbsession, ranking_tag):
    return dbsession.query(Record, User).filter_by(ranking_tag=ranking_tag).filter(Record.user_hash == User.user_hash).order_by(Record.like_count, Record.round_score, desc(Record.datetime)).all()


def _ranking_file(dbsession, ranking_tag, user_hash, path):
    with open(path) as f:
        dat_text = f.read()

    return dat_text


@view_config(route_name='home')
def home(request):
    return HTTPFound(location='/ranking')


@view_config(route_name='user_login', renderer='../templates/user_login.jinja2')
def user_login(request):
    try:
        login_url = request.route_url('user_login')
        referrer = request.url
        if referrer == login_url:
            referrer = '/'
        came_from = request.params.get('came_from', referrer)
        user_hash = request.params.get('id', '')
        password = request.params.get('password', '')
        if request.params.get('login'):
            if check_password(user_hash, password, request.dbsession):
                headers = remember(request, user_hash)
                return HTTPFound(location=came_from, headers=headers)

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {
        'came_from': came_from,
        'id': user_hash,
        'password': password,
    }


@view_config(route_name='user_logout')
def user_logout(request):
    try:
        headers = forget(request)
        url = request.route_url('home')
    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return HTTPFound(location=url, headers=headers)


@view_config(route_name='ranking', renderer='../templates/ranking.jinja2')
def ranking(request):
    try:
        ranking_tag = request.params.get('tag', None)

        dbsession = request.dbsession
        ranks = dbsession.query(Ranking).order_by(desc(Ranking.created_at)).all()

        records = []
        if ranking_tag:
            records = _ranking_records(dbsession, ranking_tag)
            records.reverse()

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {
        'userid': request.authenticated_userid,
        'ranks': ranks,
        'records': records,
        'ranking_tag': ranking_tag,
    }


@view_config(route_name='ranking_new', renderer='../templates/ranking_new.jinja2', permission='member')
def ranking_new(request):
    return {
        'userid': request.authenticated_userid,
    }


@view_config(route_name='ranking_add', renderer='../templates/ranking_add.jinja2', permission='member')
def ranking_add(request):
    try:
        dat_file = request.POST['dat'].file
        title = request.POST['title'].strip()
        description = request.POST['description'].strip()
        open_date = dateutil.parser.parse(request.POST['open_date'])
        start_date = dateutil.parser.parse(request.POST['start_date'])
        end_date = dateutil.parser.parse(request.POST['end_date'])
        close_date = dateutil.parser.parse(request.POST['close_date'])
        hour = int(request.POST['hour'])

        while True:
            tag = _random_token(16)
            path = os.path.join(Rankings_directory, tag + '.dat')
            if not os.path.exists(path):
                break
        _save_dat_with_tag(dat_file, path, tag)

        time_hour = timedelta(hours=hour)
        ranking = Ranking(ranking_tag=tag,
                          title=title, description=description,
                          open_datetime=open_date + time_hour,
                          start_datetime=start_date + time_hour,
                          end_datetime=end_date + time_hour,
                          close_datetime=close_date + time_hour)
        request.dbsession.add(ranking)

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return HTTPFound(location='/ranking')


@view_config(route_name='user_add')
def user_add(request):
    try:
        # user_hash, passphrase を受け取って登録する。
        user_hash = request.POST['id']
        passphrase = request.POST['passphrase']
        nickname = request.POST['nickname']

        dbsession = request.dbsession
        if _check_passphrase(dbsession, user_hash, passphrase.encode('utf8')):
            # 認証できた場合は OK を返す。
            response = Server_ok

        else:
            result = dbsession.query(User).filter_by(user_hash=user_hash).one_or_none()
            if result:
                # id だけ一致する場合にはユーザ登録済みのメッセージを返す。
                response = 'Fail: already exists.'
            else:
                # レコードが存在しない場合には、新規ユーザとして登録する。
                salt = bcrypt.gensalt(rounds=10, prefix=b'2b')
                passphrase = bcrypt.hashpw(passphrase.encode('utf8'), salt)
                user = User(user_hash=user_hash,
                            passphrase=passphrase, nickname=nickname)

                # admin が登録されてないときには admin として登録する。
                users = dbsession.query(User).filter_by(is_admin=True).all()
                if len(users) == 0:
                    user.is_admin = True

                dbsession.add(user)
                response = Server_ok

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)

    return Response(response, content_type='text/plain')


@view_config(route_name='user_token')
def user_token(request):
    try:
        # user_hash を受け取り、トークンを生成して保存する。
        user_hash = request.POST['id']
        user_token = _random_token(48)

        # 既存のレコードを削除する。
        request.dbsession.query(Token).filter_by(user_hash=user_hash).delete()

        # DB にトークンを追加する。
        token = Token(user_hash=user_hash, value=user_token)
        request.dbsession.add(token)

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(user_token, content_type='text/plain')


@view_config(route_name='user_nickname')
def user_nickname(request):
    try:
        # user_hash を受け取り nickname を返す。
        user_hash = request.POST['id']
        result = request.dbsession.query(User).filter_by(user_hash=user_hash).one_or_none()
        if not result:
            nickname = 'unknown'
        else:
            nickname = result.nickname

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)

    return Response(nickname, content_type='text/plain')


@view_config(route_name='user_nickname_update')
def user_nickname_update(request):
    try:
        # user_hash と encrypted token を受け取り、decrypt して比較する。
        user_hash = request.POST['id']
        encrypted_passphrase = base64.b64decode(request.POST['passphrase'])
        nickname = request.POST['nickname']

        dbsession = request.dbsession
        if not _authenticate(dbsession, user_hash, encrypted_passphrase):
            return Response('Fail', content_type='text/plain')

        user = dbsession.query(User).filter_by(user_hash=user_hash).one_or_none()
        if user:
            user.nickname = nickname

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(Server_ok, content_type='text/plain')


@view_config(route_name='ranking_list')
def ranking_list(request):
    try:
        dbsession = request.dbsession
        now = datetime.now()
        rankings = dbsession.query(Ranking).filter(Ranking.open_datetime < now, Ranking.close_datetime > now).order_by(Ranking.open_datetime).all()
        data = []
        for ranking in rankings:
            data.append({
                'ranking_tag': ranking.ranking_tag,
                'title': ranking.title,
                'description': ranking.description,
                'open_datetime': str(ranking.open_datetime),
                'start_datetime': str(ranking.start_datetime),
                'end_datetime': str(ranking.end_datetime),
                'close_datetime': str(ranking.close_datetime),
                'is_after_download': ranking.is_after_download,
            })
        json_text = json.dumps({'rankings': data})

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(json_text, content_type='text/plain')


@view_config(route_name='ranking_information')
def ranking_information(request):
    try:
        ranking_tag = request.matchdict.get('ranking_tag')

        dbsession = request.dbsession

        users = []
        records = _ranking_records(dbsession, ranking_tag)
        for item in records:
            line = '{},{},{},{}'.format(item.Record.user_hash, item.Record.round_score, item.Record.like_count, item.User.nickname)
            users.append(line)

        ranking = dbsession.query(Ranking).filter_by(ranking_tag=ranking_tag).first()
        data = {
            'tag': ranking.ranking_tag,
            'title': ranking.title,
            'description': ranking.description,
            'open_datetime': str(ranking.open_datetime),
            'start_datetime': str(ranking.start_datetime),
            'end_datetime': str(ranking.end_datetime),
            'close_datetime': str(ranking.close_datetime),
            'is_after_download': ranking.is_after_download,

            'users': users,
        }
        json_text = json.dumps(data)

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(json_text, content_type='text/plain')


@view_config(route_name='ranking_upload')
def ranking_upload(request):
    try:
        ranking_tag = request.matchdict.get('ranking_tag')
        user_hash = request.POST['id']
        encrypted_passphrase = base64.b64decode(request.POST['passphrase'])

        # 認証する。
        dbsession = request.dbsession
        if not _authenticate(dbsession, user_hash, encrypted_passphrase):
            return Response('Fail', content_type='text/plain')

        # .dat ファイルをチェックして到達ラウンド数を取得する。
        dat_file = request.POST['dat'].file
        yaml_dat = yaml.load(dat_file, Loader=yaml.SafeLoader)
        dat_file.seek(0)
        round_score = yaml_dat['Round']

        # .rep ファイルは未チェックで保存する。
        rep_file = request.POST['rep'].file

        # ファイルを保存するためのディレクトリを作成してファイルを配置する。
        encoded_user_hash = user_hash.replace('/', '_2f')
        directory = os.path.join(Rankings_directory, encoded_user_hash)
        file_base = os.path.join(directory, ranking_tag)
        os.makedirs(directory, exist_ok=True)
        _save_uploaded_file(dat_file, file_base + '.dat')
        _save_uploaded_file(rep_file, file_base + '.dat.rep')

        # 到達レコードの情報を DB に保存する。
        record = request.dbsession.query(Record).filter_by(ranking_tag=ranking_tag, user_hash=user_hash).one_or_none()
        if record:
            record.round_score = round_score
        else:
            record = Record(ranking_tag=ranking_tag, user_hash=user_hash, round_score=round_score)
            request.dbsession.add(record)

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(Server_ok, content_type='text/plain')


@view_config(route_name='ranking_map')
def ranking_map(request):
    try:
        user_hash = request.params.get('id', '')
        ranking_tag = request.matchdict.get('ranking_tag')

        dbsession = request.dbsession
        record = dbsession.query(Record).filter_by(user_hash=user_hash, ranking_tag=ranking_tag).one_or_none()
        if not record:
            # レコードがなければゼロスコアで登録を行う。
            zero_record = Record(ranking_tag=ranking_tag, user_hash=user_hash, round_score=0)
            dbsession.add(zero_record)

        path = os.path.join(Rankings_directory, ranking_tag + '.dat')
        with open(path) as f:
            dat_text = f.read()

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(dat_text, content_type='text/plain')


@view_config(route_name='ranking_dat')
def ranking_dat(request):
    try:
        ranking_tag = request.matchdict.get('ranking_tag')
        user_hash = request.matchdict.get('user_hash')

        encoded_user_hash = user_hash.replace('/', '_2f')
        path = os.path.join(Rankings_directory, encoded_user_hash, ranking_tag + '.dat')
        dat_text = _ranking_file(request.dbsession, ranking_tag, user_hash, path)

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(dat_text, content_type='text/plain')


@view_config(route_name='ranking_rep')
def ranking_rep(request):
    try:
        ranking_tag = request.matchdict.get('ranking_tag')
        user_hash = request.matchdict.get('user_hash')

        encoded_user_hash = user_hash.replace('/', '_2f')
        path = os.path.join(Rankings_directory, encoded_user_hash, ranking_tag + '.dat.rep')
        dat_text = _ranking_file(request.dbsession, ranking_tag, user_hash, path)

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(dat_text, content_type='text/plain')

@view_config(route_name='ranking_likes')
def ranking_likes(request):
    try:
        ranking_tag = request.matchdict.get('ranking_tag')
        user_hash = request.params.get('id')

        likes = request.dbsession.query(LikeLog).filter_by(ranking_tag=ranking_tag, from_user_hash=user_hash).all()

        data = []
        for like in likes:
            data.append(like.to_user_hash)
        json_text = json.dumps({'users': data})

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(json_text, content_type='text/plain')


def _update_like_count(dbsession, ranking_tag, to_user_hash):
    ranking = dbsession.query(Ranking).filter_by(ranking_tag=ranking_tag).one_or_none()
    now = datetime.now()
    if now >= ranking.end_datetime:
        return

    record = dbsession.query(Record).filter_by(ranking_tag=ranking_tag, user_hash=to_user_hash).one()
    likes = dbsession.query(LikeLog).filter_by(ranking_tag=ranking_tag, to_user_hash=to_user_hash).all()
    record.like_count = len(likes)


@view_config(route_name='ranking_like')
def ranking_like(request):
    try:
        ranking_tag = request.matchdict.get('ranking_tag')
        from_user_hash = request.params.get('id')
        to_user_hash = request.params.get('to_user')
        encrypted_passphrase = base64.b64decode(request.POST['passphrase'])
        dbsession = request.dbsession
        if not _authenticate(dbsession, from_user_hash, encrypted_passphrase):
            return Response('Fail', content_type='text/plain')

        record = request.dbsession.query(LikeLog).filter_by(ranking_tag=ranking_tag, from_user_hash=from_user_hash, to_user_hash=to_user_hash).one_or_none()
        if not record:
            like_log = LikeLog(ranking_tag=ranking_tag, from_user_hash=from_user_hash, to_user_hash=to_user_hash)
            request.dbsession.add(like_log)
            _update_like_count(request.dbsession, ranking_tag, to_user_hash)

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(Server_ok, content_type='text/plain')


@view_config(route_name='ranking_dislike')
def ranking_dislike(request):
    try:
        ranking_tag = request.matchdict.get('ranking_tag')
        from_user_hash = request.params.get('id')
        to_user_hash = request.params.get('to_user')
        encrypted_passphrase = base64.b64decode(request.POST['passphrase'])
        dbsession = request.dbsession
        if not _authenticate(dbsession, from_user_hash, encrypted_passphrase):
            return Response('Fail', content_type='text/plain')

        request.dbsession.query(LikeLog).filter_by(ranking_tag=ranking_tag, from_user_hash=from_user_hash, to_user_hash=to_user_hash).delete()

        _update_like_count(request.dbsession, ranking_tag, to_user_hash)

    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return Response(Server_ok, content_type='text/plain')


db_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to run the "initialize_bc_server_db" script
    to initialize your database tables.  Check your virtual
    environment's "bin" directory for this script and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""
