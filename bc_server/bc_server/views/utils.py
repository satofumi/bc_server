from datetime import datetime


def format_date(value):
    return value.strftime('%Y-%m-%d')


def format_datetime(value):
    return value.strftime('%Y-%m-%d %H:%M')
