#!/bin/bash

source ./env/bin/activate
nohup pserve production.ini --reload > out.txt 2> err.txt &
ps > last_ps.txt
